export interface PropertyMapItemImage {
	url: string;
}

export interface Position {
	latitude: number;
	longitude: number;
}
export interface PropertyMapItem {
	name: string;
	images: PropertyMapItemImage[];
	distanceFromCityCenter: number;
	rating: number;
	lowestPricePerNight: number;
	position: Position;
	id: number;
}
export interface BedroomSuites {
	bedRoomCount: number;
	maxGuests: number;
	id: number;
}
export interface PropertyDetails {
	name: string;
	images: PropertyMapItemImage[];
	distanceFromCityCenter: number;
	rating: number;
	lowestPricePerNight: number;
	position: Position;
	id: number;
	description: string;
	bedroomSuites: BedroomSuites[];
}
