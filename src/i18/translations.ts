export const translations = {
	en: {
		search: "Search",
		map: "Map",
		saved: "Saved",
		profile: "Profile",
		exploreButton: "EXPLORE",
		distanceFromCenter: "km from city center",
		night: "Night",
		from: "From",
		availableRoomTypes: "Room types available in this location",
		bedRoomSuites: "Bedroom suites"
	}
};
