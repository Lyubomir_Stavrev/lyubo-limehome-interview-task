import { PropertyMapItem } from "../../common/propertyTypes";
import { AllPropertiesAPIResponse } from "../../common/types";
import * as services from "../services";

const apiResObject: AllPropertiesAPIResponse = {
	message: "",
	payload: [
		{
			id: 99,
			external_id: "BERLIN",
			review_widget_id: null,
			name: "berlin schlüterstraße",
			city: "berlin",
			city_id: 32,
			street: "schlüterstraße",
			location: {
				lat: 52.50176279999999,
				lng: 13.3164893,
				city: "Berlin",
				postalCode: "10629",
				countryCode: "DE",
				addressLine1: "Schlüterstraße 39",
				countryName: "Germany"
			},
			distance: 6.321157219990815,
			images: [
				{
					url: "https://limehome.imgix.net/properties/99/9fee5516-9d41-41f7-95ea-6ac72c49fb86.jpg",
					is_portrait: false,
					position: 0,
					unit_group_ids: [16],
					tags: []
				}
			],
			lowest_price_per_night: 100,
			lowest_price_per_month: null,
			unit_groups: [
				{
					id: 16,
					bedroom_count: 1,
					max_guests: 1
				}
			]
		}
	],
	success: false
};

describe("testing services", () => {
	const mockFetch = (global.fetch = jest.fn());

	beforeEach(() => {
		mockFetch.mockClear();
	});
	it("should map the paload object correctly", async () => {
		mockFetch.mockImplementationOnce(() => {
			return new Promise((resolve) => {
				resolve({
					ok: true,
					status: 201,
					json: () => {
						return apiResObject;
					}
				});
			});
		});
		jest.spyOn(services, "fetchAllProperties").mockReturnValue(
			new Promise((resolve) => {
				resolve(apiResObject);
			})
		);
		const propertyMapItem: PropertyMapItem[] = [
			{
				name: apiResObject.payload[0].name,
				images: apiResObject.payload[0].images.map((img) => ({
					url: img.url
				})),
				distanceFromCityCenter: Number(
					apiResObject.payload[0].distance.toFixed(1)
				),
				rating: 4.5,
				lowestPricePerNight:
					apiResObject.payload[0].lowest_price_per_night,
				position: {
					latitude: apiResObject.payload[0].location.lat,
					longitude: apiResObject.payload[0].location.lng
				},
				id: apiResObject.payload[0].id
			}
		];

		mockFetch.mockImplementation(
			() =>
				new Promise((resolve) => {
					resolve({
						ok: true,
						status: 201,
						json: () => {
							return propertyMapItem;
						}
					});
				})
		);

		const rooms = await services.getMappedProperties();

		expect(rooms).toEqual(propertyMapItem);
	});
});
