import { PropertyMapItem, PropertyDetails } from "../common/propertyTypes";
import {
	AllPropertiesAPIResponse,
	SinglePropertyAPIResponse
} from "../common/types";

const propertyApiHref = `https://api.limehome.com/properties/v1/public/properties`;

export const fetchAllProperties = async () => {
	const data = await fetch(propertyApiHref);

	const apartmentsJson: AllPropertiesAPIResponse = await data.json();

	return apartmentsJson;
};

const fetchOneProperty = async (id: number) => {
	const data = await fetch(`${propertyApiHref}/${id}`);
	const apartmentsJson: SinglePropertyAPIResponse = await data.json();

	return apartmentsJson;
};
export const getMappedProperties = async (): Promise<PropertyMapItem[]> => {
	const result = await fetchAllProperties();
	const mappedItems = result.payload?.map((property) => {
		const propertyMapItem: PropertyMapItem = {
			name: property.name,
			images: property.images.map((image) => ({ url: image.url })),
			distanceFromCityCenter: Number(property.distance.toFixed(1)),
			rating: 4.5, //just for demo (doesn't exist in the given api)
			lowestPricePerNight:
				property.lowest_price_per_night ||
				Math.floor(Math.random() * (200 - 30 + 1) + 30), //just for demo (api returns null)
			position: {
				latitude: property.location.lat,
				longitude: property.location.lng
			},
			id: property.id
		};
		return propertyMapItem;
	});
	return mappedItems;
};

export const getPropertyDetails = async (
	propertyId: number
): Promise<PropertyDetails> => {
	const result = await fetchOneProperty(propertyId);
	const property = result.payload;
	const propertyItem = {
		name: property.name,
		images: property.images.map((image) => ({ url: image.url })),
		distanceFromCityCenter: Number(property.distance.toFixed(1)),
		rating: 4.5, //just for demo (doesn't exist in the given api)
		lowestPricePerNight:
			property.lowest_price_per_night ||
			Math.floor(Math.random() * (200 - 30 + 1) + 30),
		position: {
			latitude: property.location.lat,
			longitude: property.location.lng
		},
		bedroomSuites: property.unit_groups.map((el) => ({
			bedRoomCount: el.bedroom_count,
			maxGuests: el.max_guests,
			id: el.id
		})),
		description: property.description,
		id: property.id
	};
	return propertyItem;
};
