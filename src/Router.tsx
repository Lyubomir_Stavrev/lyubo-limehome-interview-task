import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import PropertyDetails from "./screens/PropertyDetails/PropertyDetails";
import Home from "./screens/Home/Home";

const DrawerOuter = createDrawerNavigator();
export default function Router() {
	return (
		<NavigationContainer>
			<DrawerOuter.Navigator>
				<DrawerOuter.Screen
					name="Home"
					component={Home}
					options={{ headerShown: false }}
				/>
				<DrawerOuter.Screen
					name="PropertyDetails"
					component={PropertyDetails}
					options={{ headerShown: false }}
				/>
			</DrawerOuter.Navigator>
		</NavigationContainer>
	);
}
