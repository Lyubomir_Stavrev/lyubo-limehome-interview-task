export const palette = {
	darkGreen: "#4d6447",
	orange: "#0ECD9D",
	green: "#c9cbab",
	white: "#f7f1e9",
	darkerWhite: "#d7d6ca",
	grey: "#9a9893",
	lighterGrey: "#959698",
	darkerGrey: "#535254",
	lightOrange: "#b26422",
	darkerOrange: "#99501d"
};
export const theme = {
	colors: {
		primeryColor: palette.white,
		secondaryColor: palette.green,
		primeryTextColor: palette.grey,
		secondaryTextColor: palette.darkerWhite,
		contrastColor: palette.darkGreen,
		active: palette.lightOrange
	},
	spacing: {
		s: 8,
		m: 16,
		l: 24,
		xl: 40
	},
	textVariants: {
		header: {
			fontFamily: "redcoat",
			fontSize: 26,
			fontWeight: "bold"
		},
		body: {
			fontFamily: "montserratSemiBold",
			fontSize: 16
		}
	}
};
