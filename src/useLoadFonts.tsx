import * as Font from "expo-font";
import { useQuery } from "react-query";
const getFonts = () =>
	Font.loadAsync({
		redcoat: require("./assets/fonts/POERedcoatNew.ttf"),
		montserratSemiBold: require("./assets/fonts/MontserratSemiBold.ttf")
	});
const loadFonts = async () => {
	return await getFonts();
};
const useLoadFonts = () => useQuery("useLoadFont", loadFonts);

export default useLoadFonts;
