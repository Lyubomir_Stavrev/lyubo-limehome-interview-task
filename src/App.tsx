import React from "react";

import { QueryClient, QueryClientProvider } from "react-query";
import { StatusBar } from "expo-status-bar";

import Router from "./Router";
import WarmUp from "./WarmUp";

const queryClient = new QueryClient();

export default function App() {
	return (
		<QueryClientProvider client={queryClient}>
			<WarmUp>
				<Router />
			</WarmUp>
			<StatusBar style="auto" />
		</QueryClientProvider>
	);
}
