import React from "react";
import Svg, { Circle, Path, Rect } from "react-native-svg";
import { palette, theme } from "../../constants/Theme";

export const SearchSvg = () => (
	<Svg
		height="30px"
		viewBox="0 0 24 20"
		width="30px"
		fill={theme.colors.primeryColor}>
		<Path d="M0 0h24v24H0V0z" fill="none" />
		<Path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
	</Svg>
);

export const StarSvg = () => (
	<Svg
		enable-background="new 0 0 24 24"
		height="24px"
		viewBox="0 0 24 24"
		width="24px"
		fill={theme.colors.active}>
		<Path d="M0 0h24v24H0V0z" fill="none" />
		<Path d="M0 0h24v24H0V0z" fill="none" />

		<Path d="m12 17.27 4.15 2.51c.76.46 1.69-.22 1.49-1.08l-1.1-4.72 3.67-3.18c.67-.58.31-1.68-.57-1.75l-4.83-.41-1.89-4.46c-.34-.81-1.5-.81-1.84 0L9.19 8.63l-4.83.41c-.88.07-1.24 1.17-.57 1.75l3.67 3.18-1.1 4.72c-.2.86.73 1.54 1.49 1.08l4.15-2.5z" />
	</Svg>
);
export const LocationSvg = () => (
	<Svg
		height="24px"
		viewBox="0 0 24 24"
		width="24px"
		fill={theme.colors.active}>
		<Path d="M0 0h24v24H0V0z" fill="none" />
		<Path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zM7 9c0-2.76 2.24-5 5-5s5 2.24 5 5c0 2.88-2.88 7.19-5 9.88C9.92 16.21 7 11.85 7 9z" />
		<Circle cx="12" cy="9" r="2.5" />
	</Svg>
);
export const CloseSvg = () => (
	<Svg
		height="24px"
		viewBox="0 0 24 24"
		width="24px"
		fill={palette.darkerGrey}>
		<Path d="M0 0h24v24H0V0z" fill="none" />
		<Path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
	</Svg>
);
export const MapSvg = () => (
	<Svg
		height="30px"
		viewBox="0 0 24 24"
		width="30px"
		fill={theme.colors.primeryColor}>
		<Path d="M0 0h24v24H0V0z" fill="none" />
		<Path d="M20.5 3l-.16.03L15 5.1 9 3 3.36 4.9c-.21.07-.36.25-.36.48V20.5c0 .28.22.5.5.5l.16-.03L9 18.9l6 2.1 5.64-1.9c.21-.07.36-.25.36-.48V3.5c0-.28-.22-.5-.5-.5zM10 5.47l4 1.4v11.66l-4-1.4V5.47zm-5 .99l3-1.01v11.7l-3 1.16V6.46zm14 11.08l-3 1.01V6.86l3-1.16v11.84z" />
	</Svg>
);
export const SavedSvg = () => (
	<Svg
		height="30px"
		viewBox="0 0 24 24"
		width="30px"
		fill={theme.colors.primeryColor}>
		<Path d="M0 0h24v24H0V0z" fill="none" />
		<Path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z" />
	</Svg>
);
export const ProfileSvg = () => (
	<Svg
		enable-background="new 0 0 24 24"
		height="30px"
		viewBox="0 0 24 24"
		width="30px"
		fill="#f7f1e9">
		<Rect fill="none" height="24" width="24" />
		<Path d="M12,2C6.48,2,2,6.48,2,12s4.48,10,10,10s10-4.48,10-10S17.52,2,12,2z M12,6c1.93,0,3.5,1.57,3.5,3.5S13.93,13,12,13 s-3.5-1.57-3.5-3.5S10.07,6,12,6z M12,20c-2.03,0-4.43-0.82-6.14-2.88C7.55,15.8,9.68,15,12,15s4.45,0.8,6.14,2.12 C16.43,19.18,14.03,20,12,20z" />
	</Svg>
);
export const ArrowRight = () => (
	<Svg
		enable-background="new 0 0 24 24"
		height="24px"
		viewBox="0 0 24 24"
		width="24px"
		fill={theme.colors.primeryColor}>
		<Rect fill="none" height="24" width="24" />
		<Path d="M15,5l-1.41,1.41L18.17,11H2V13h16.17l-4.59,4.59L15,19l7-7L15,5z" />
	</Svg>
);
