import {
	StyleSheet,
	View,
	Text,
	Image,
	ScrollView,
	SafeAreaView,
	NativeScrollEvent
} from "react-native";

import React, { useState } from "react";
import { PropertyMapItemImage } from "../../common/propertyTypes";

import layout from "../../constants/Layout";

interface ImageSliderProps {
	images: PropertyMapItemImage[];
}
const dot = "●";
export default function ImageSlider({ images }: ImageSliderProps) {
	const [imgActive, setImgActive] = useState<number>();
	const onChange = (nativeEvent: NativeScrollEvent) => {
		if (nativeEvent) {
			const slide = Math.ceil(
				nativeEvent.contentOffset.x /
					nativeEvent.layoutMeasurement.width
			);
			if (slide != imgActive) {
				setImgActive(slide);
			}
		}
	};
	return (
		<SafeAreaView style={styles.container}>
			<View style={styles.wrap}>
				<ScrollView
					onScroll={({ nativeEvent }) => onChange(nativeEvent)}
					showsHorizontalScrollIndicator={false}
					scrollEventThrottle={0}
					pagingEnabled
					horizontal
					style={styles.wrap}>
					{images?.map((img) => (
						<Image
							key={img.url}
							style={styles.wrap}
							source={{
								uri: img?.url
							}}
						/>
					))}
				</ScrollView>
				<View style={styles.wrapDot}>
					{images?.map((img, index) => (
						<Text
							key={img.url}
							style={
								imgActive === index
									? styles.dotActive
									: styles.dot
							}>
							{dot}
						</Text>
					))}
				</View>
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	wrap: {
		width: layout.window.width,
		height: layout.window.height * 0.3
	},
	wrapDot: {
		position: "absolute",
		bottom: 0,
		flexDirection: "row",
		alignSelf: "center"
	},
	dotActive: {
		margin: 3,
		color: "black"
	},
	dot: {
		margin: 3,
		color: "white"
	}
});
