import { StyleSheet, View, Text } from "react-native";

import React from "react";
import { I18n } from "i18n-js";
import { BedroomSuites } from "../../common/propertyTypes";
import { theme, palette } from "../../constants/Theme";

interface RoomAvailableTypesProps {
	i18n: I18n;
	bedroomSuites: BedroomSuites[];
}

export default function RoomAvailableTypes({
	i18n,
	bedroomSuites
}: RoomAvailableTypesProps) {
	return (
		<>
			<Text style={styles.bottomTitle}>
				{i18n.t("availableRoomTypes")}
			</Text>
			<View style={styles.bottomInfoContainer}>
				{bedroomSuites?.map((suit) => (
					<View key={suit.id} style={styles.roomTypesContainer}>
						<Text style={styles.roomTypesText}>
							{suit.bedRoomCount}x{suit.maxGuests}{" "}
							{i18n.t("bedRoomSuites")}
						</Text>
					</View>
				))}
			</View>
		</>
	);
}

const styles = StyleSheet.create({
	roomTypesContainer: {
		padding: 5,
		paddingLeft: 9,
		paddingRight: 9,
		backgroundColor: theme.colors.secondaryColor,
		marginTop: 10,
		marginRight: 7,
		borderRadius: 5
	},
	roomTypesText: {
		fontSize: 15,
		color: palette.darkerGrey,
		fontFamily: "montserratSemiBold"
	},
	bottomTitle: {
		fontSize: 20,
		fontFamily: theme.textVariants.header.fontFamily,
		fontWeight: "900",
		color: palette.darkerGrey
	},
	bottomInfoContainer: {
		flex: 1,
		flexDirection: "row",
		flexWrap: "wrap",
		alignItems: "flex-start"
	}
});
