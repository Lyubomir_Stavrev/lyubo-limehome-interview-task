import { StyleSheet, View, Text, TouchableOpacity } from "react-native";

import React from "react";
import { PropertyDetails } from "../../common/propertyTypes";
import i18n from "../../i18";

import { LocationSvg, StarSvg } from "../../assets/svgs";

import RoomAvailableTypes from "./RoomAvailableTypes";
import { theme, palette } from "../../constants/Theme";
import Layout from "../../constants/Layout";

interface PropertyContantProps {
	propertyDetails: PropertyDetails;
}

export default function PropertyContant({
	propertyDetails
}: PropertyContantProps) {
	return (
		<View style={styles.contantContainer}>
			<View style={styles.titleContainer}>
				<Text style={styles.infoTitle}>{propertyDetails.name}</Text>
				<TouchableOpacity style={styles.ratingContainer}>
					<Text style={styles.ratingText}>
						{propertyDetails.rating}
					</Text>
					<StarSvg />
				</TouchableOpacity>
			</View>
			<View style={styles.topInfoContainer}>
				<View style={styles.subTitle}>
					<LocationSvg />
					<Text style={styles.topSmallInfoText}>
						{propertyDetails.distanceFromCityCenter}{" "}
						{i18n.t("distanceFromCenter")}
					</Text>
				</View>
				<View>
					<Text style={styles.topSmallInfoText}>
						{propertyDetails.description}
					</Text>
				</View>
			</View>
			<RoomAvailableTypes
				i18n={i18n}
				bedroomSuites={propertyDetails.bedroomSuites}
			/>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.colors.primeryColor
	},
	textContainer: { maxHeight: Layout.window.height * 0.9 },
	closeButton: {
		zIndex: 100,
		backgroundColor: theme.colors.primeryColor,
		padding: 10,
		position: "absolute",
		left: "5%",
		top: "5%"
	},
	subTitle: { flexDirection: "row", marginBottom: 20 },
	roomTypesContainer: {
		padding: 5,
		paddingLeft: 9,
		paddingRight: 9,
		backgroundColor: theme.colors.secondaryColor,
		marginTop: 10,
		marginRight: 7,
		borderRadius: 5
	},
	roomTypesText: {
		fontSize: 15
	},
	bottomTitle: {
		fontSize: 20,
		fontWeight: "500"
	},
	imageContainer: { position: "absolute" },
	propertyImage: {
		width: Layout.window.width,
		height: Layout.window.height * 0.2
	},

	infoTitle: {
		fontSize: 30,
		color: palette.darkerGrey,
		fontFamily: theme.textVariants.header.fontFamily,
		marginBottom: 5
	},
	contantContainer: {
		width: Layout.window.width,
		top: 10,
		padding: 15
	},
	ratingContainer: {
		borderWidth: 1,
		justifyContent: "space-between",
		alignItems: "center",
		position: "absolute",
		right: 0,
		top: 5,
		zIndex: 1,
		backgroundColor: theme.colors.primeryColor,
		width: 65,
		paddingLeft: 6,
		paddingRight: 6,
		height: 30,
		flexDirection: "row"
	},
	ratingText: {
		fontWeight: "600"
	},
	topInfoContainer: {
		borderBottomWidth: 0.5,
		paddingBottom: 15,
		width: Layout.window.width - 40,
		color: palette.darkerGrey,
		marginBottom: 15,
		flexDirection: "column",
		justifyContent: "space-around"
	},
	bottomInfoContainer: {
		flex: 1,
		flexDirection: "row",
		flexWrap: "wrap",
		alignItems: "flex-start"
	},
	topSmallInfoText: {
		fontSize: 17,
		color: palette.darkerGrey,
		fontWeight: "400",
		letterSpacing: 1,
		fontFamily: theme.textVariants.body.fontFamily
	},
	titleContainer: {
		flexDirection: "row",
		height: 50
	},
	infoText: { color: palette.darkerGrey, fontSize: 20 }
});
