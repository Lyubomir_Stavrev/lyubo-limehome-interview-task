import { StyleSheet, View, Text, TouchableOpacity } from "react-native";

import React from "react";
import { I18n } from "i18n-js";

import { theme, palette } from "../../constants/Theme";
import { ArrowRight } from "../../assets/svgs";
import Layout from "../../constants/Layout";

interface FooterProps {
	lowestPricePerNight: number;
	i18n: I18n;
}

export default function PropertyDetailsFooter({
	i18n,
	lowestPricePerNight
}: FooterProps) {
	return (
		<View style={styles.footer}>
			<View style={styles.infoContainer}>
				<Text style={styles.greyInfoText}>{i18n.t("from")} </Text>
				<Text style={styles.bottomInfoText}>
					{lowestPricePerNight}
					€/{i18n.t("night")}
				</Text>
			</View>
			<TouchableOpacity style={styles.exploreButton}>
				<Text style={styles.exploreButtonText}>
					{i18n.t("exploreButton")}
				</Text>
				<ArrowRight />
			</TouchableOpacity>
		</View>
	);
}

const styles = StyleSheet.create({
	infoContainer: {
		flex: 1,
		flexDirection: "row",
		flexWrap: "wrap",
		alignItems: "flex-start",
		marginTop: 5
	},
	footer: {
		height: 70,
		width: Layout.window.width,
		padding: 10,
		backgroundColor: theme.colors.secondaryColor,
		position: "absolute",
		bottom: 0,
		justifyContent: "center"
	},
	exploreButton: {
		position: "absolute",
		right: 20,
		top: 10,
		padding: 10,
		width: 155,
		maxWidth: 250,
		borderRadius: 3,
		backgroundColor: palette.darkGreen,
		justifyContent: "center",
		alignItems: "center",
		flexDirection: "row"
	},
	exploreButtonText: {
		fontSize: 17,
		color: theme.colors.primeryColor,
		fontWeight: "400",
		letterSpacing: 1,
		marginRight: 10
	},
	greyInfoText: { color: palette.darkerGrey, fontSize: 20 },

	bottomInfoText: {
		color: theme.colors.active,
		fontSize: 20
	}
});
