import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	ScrollView
} from "react-native";

import React from "react";
import { PropertyMapItem } from "../../common/propertyTypes";
import i18n from "../../i18";

import { LocationSvg, StarSvg, CloseSvg } from "../../assets/svgs";

import { useGetPropertyDetails } from "./useGetMapPropertyDetails";
import ImageSlider from "./ImageSlider";
import PropertyDetailsFooter from "./PropertyDetailsFooter";
import RoomAvailableTypes from "./RoomAvailableTypes";
import { theme, palette } from "../../constants/Theme";
import Layout from "../../constants/Layout";
import PropertyContant from "./PropertyContant";

interface PropertyDetailsProps {
	markerInfo: PropertyMapItem;
	navigation: {
		navigate: (text: string, params?: { propertyId: number }) => void;
	};
	route: { params: { propertyId: number } };
}

export default function PropertyDetails({
	route,
	navigation
}: PropertyDetailsProps) {
	const propertyId = route.params?.propertyId;
	const propertyDetails = useGetPropertyDetails(propertyId);

	return (
		<View style={styles.container}>
			<TouchableOpacity
				onPress={() => navigation.navigate("Home")}
				style={styles.closeButton}>
				<CloseSvg />
			</TouchableOpacity>
			{propertyDetails.data ? (
				<>
					<ScrollView style={styles.textContainer}>
						<ImageSlider images={propertyDetails.data.images} />
						<PropertyContant
							propertyDetails={propertyDetails.data}
						/>
					</ScrollView>
					<PropertyDetailsFooter
						i18n={i18n}
						lowestPricePerNight={
							propertyDetails.data.lowestPricePerNight
						}
					/>
				</>
			) : null}
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.colors.primeryColor
	},
	textContainer: { maxHeight: Layout.window.height * 0.9 },
	closeButton: {
		zIndex: 100,
		backgroundColor: theme.colors.primeryColor,
		padding: 10,
		position: "absolute",
		left: "5%",
		top: "5%"
	},
	subTitle: { flexDirection: "row", marginBottom: 20 },
	roomTypesContainer: {
		padding: 5,
		paddingLeft: 9,
		paddingRight: 9,
		backgroundColor: theme.colors.secondaryColor,
		marginTop: 10,
		marginRight: 7,
		borderRadius: 5
	},
	roomTypesText: {
		fontSize: 15
	},
	bottomTitle: {
		fontSize: 20,
		fontWeight: "500"
	},
	imageContainer: { position: "absolute" },
	propertyImage: {
		width: Layout.window.width,
		height: Layout.window.height * 0.2
	},

	infoTitle: {
		fontSize: 30,
		color: palette.darkerGrey,
		fontFamily: theme.textVariants.header.fontFamily,
		marginBottom: 5
	},
	contantContainer: {
		width: Layout.window.width,
		top: 10,
		padding: 15
	},
	ratingContainer: {
		borderWidth: 1,
		justifyContent: "space-between",
		alignItems: "center",
		position: "absolute",
		right: 0,
		top: 5,
		zIndex: 1,
		backgroundColor: theme.colors.primeryColor,
		width: 65,
		paddingLeft: 6,
		paddingRight: 6,
		height: 30,
		flexDirection: "row"
	},
	ratingText: {
		fontWeight: "600"
	},
	topInfoContainer: {
		borderBottomWidth: 0.5,
		paddingBottom: 15,
		width: Layout.window.width - 40,
		color: palette.darkerGrey,
		marginBottom: 15,
		flexDirection: "column",
		justifyContent: "space-around"
	},
	bottomInfoContainer: {
		flex: 1,
		flexDirection: "row",
		flexWrap: "wrap",
		alignItems: "flex-start"
	},
	topSmallInfoText: {
		fontSize: 17,
		color: palette.darkerGrey,
		fontWeight: "400",
		letterSpacing: 1,
		fontFamily: theme.textVariants.body.fontFamily
	},
	titleContainer: {
		flexDirection: "row",
		height: 50
	},
	infoText: { color: palette.darkerGrey, fontSize: 20 }
});
