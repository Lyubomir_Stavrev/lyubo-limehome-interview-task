import { useQuery } from "react-query";
import { getPropertyDetails } from "../../services/services";

export const useGetPropertyDetails = (id: number) => {
	const key = `getProperty-${id}`;
	const getProperty = async () => {
		const property = await getPropertyDetails(id);
		return property;
	};
	const { isLoading, error, data, isError, isSuccess } = useQuery(
		key,
		getProperty
	);

	return { isLoading, data, error, isError, isSuccess };
};
