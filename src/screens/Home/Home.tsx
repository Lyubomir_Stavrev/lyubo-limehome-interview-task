import React from "react";
import { StyleSheet, View } from "react-native";
import Map from "../Map/Map";
import i18n from "../../i18";
import Footer from "../Footer/Footer";

interface HomeProps {
	navigation: {
		navigate: (text: string, params?: { propertyId: number }) => void;
	};
}
export default function Home({ navigation }: HomeProps) {
	return (
		<View accessibilityLabel="home" style={styles.container}>
			<Map i18n={i18n} navigation={navigation} />
			<Footer i18n={i18n} />
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	}
});
