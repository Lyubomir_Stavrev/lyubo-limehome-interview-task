import { StyleSheet, View, Text } from "react-native";
import React from "react";

import { I18n } from "i18n-js/typings";

import { SearchSvg, MapSvg, SavedSvg, ProfileSvg } from "../../assets/svgs";
import Layout from "../../constants/Layout";
interface FooterProps {
	i18n: I18n;
}

export default function Footer({ i18n }: FooterProps) {
	return (
		<View style={styles.container}>
			<View style={styles.imageContainer}>
				<SearchSvg />
				<Text style={styles.textStyle}>{i18n.t("search")}</Text>
			</View>
			<View style={styles.imageContainer}>
				<MapSvg />
				<Text style={styles.textStyle}>{i18n.t("map")}</Text>
			</View>
			<View style={styles.imageContainer}>
				<SavedSvg />
				<Text style={styles.textStyle}>{i18n.t("saved")}</Text>
			</View>
			<View style={styles.imageContainer}>
				<ProfileSvg />
				<Text style={styles.textStyle}>{i18n.t("profile")}</Text>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		paddingLeft: 40,
		paddingRight: 40,
		position: "absolute",
		bottom: 0,
		width: Layout.window.width,
		height: 70,
		backgroundColor: "#4d6447",
		borderTopLeftRadius: 16,
		borderTopRightRadius: 16,
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	imageContainer: {
		justifyContent: "center",
		alignItems: "center"
	},
	image: {
		width: 23,
		height: 23
	},
	textStyle: {
		fontSize: 15,
		fontWeight: "300",
		color: "#ffff"
	}
});
