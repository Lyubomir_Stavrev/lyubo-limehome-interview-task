import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";

import React from "react";
import { I18n } from "i18n-js";
import { PropertyMapItem } from "../../common/propertyTypes";

import { StarSvg, LocationSvg } from "../../assets/svgs";
import { theme, palette } from "../../constants/Theme";
import layout from "../../constants/Layout";

interface MarkerInfoProps {
	i18n: I18n;
	markerInfo: PropertyMapItem;
	navigation: {
		navigate: (text: string, params?: { propertyId: number }) => void;
	};
}

export default function MarkerInfo({
	i18n,
	markerInfo,
	navigation
}: MarkerInfoProps) {
	return (
		<TouchableOpacity
			onPress={() =>
				navigation.navigate("PropertyDetails", {
					propertyId: markerInfo.id
				})
			}
			style={styles.container}>
			<View>
				<View style={styles.imageContainer}>
					<View style={styles.ratingContainer}>
						<Text style={styles.topSmallInfoText}>
							{markerInfo.rating}
						</Text>
						<StarSvg />
					</View>
					<Image
						style={styles.propertyImage}
						source={{
							uri: markerInfo.images[0].url
						}}
					/>
				</View>
				<View style={styles.markerInfoContainer}>
					<Text
						numberOfLines={1}
						ellipsizeMode="tail"
						style={styles.infoTitle}>
						{markerInfo.name}
					</Text>
					<View style={styles.topInfoContainer}>
						<LocationSvg />
						<Text style={styles.topSmallInfoText}>
							{markerInfo.distanceFromCityCenter}{" "}
							{i18n.t("distanceFromCenter")}
						</Text>
					</View>
					<View style={styles.bottomInfoContainer}>
						<Text>From </Text>
						<Text style={styles.bottomSmallInfoText}>
							{markerInfo.lowestPricePerNight}€/Night
						</Text>
					</View>
				</View>
			</View>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	container: {
		position: "absolute",
		bottom: "10%",
		backgroundColor: theme.colors.primeryColor,
		borderWidth: 1,
		borderColor: palette.grey,
		width: layout.window.width - 30,
		height: 120
	},
	imageContainer: { position: "absolute" },
	propertyImage: { width: 119, height: 119 },

	infoTitle: {
		fontSize: 30,
		color: palette.darkerGrey,
		fontFamily: "redcoat",
		marginBottom: 5
	},
	markerInfoContainer: {
		right: "5%",
		justifyContent: "center",
		alignItems: "flex-start",
		position: "absolute",
		width: 210,
		top: 10
	},
	ratingContainer: {
		justifyContent: "space-between",
		alignItems: "center",
		position: "absolute",
		right: 8,
		top: 8,
		zIndex: 1,
		backgroundColor: theme.colors.primeryColor,
		width: 55,
		padding: 4,
		height: 27,
		borderRadius: 5,
		flex: 1,
		flexDirection: "row"
	},
	markerStyle: {
		backgroundColor: palette.darkerGrey,
		padding: 10,
		paddingTop: 11,
		paddingBottom: 11,
		borderRadius: 5
	},
	arrowBorder: {
		backgroundColor: "transparent",
		borderColor: "transparent",
		borderTopColor: palette.darkerGrey,
		borderWidth: 16,
		alignSelf: "center",
		marginTop: -10.5,
		zIndex: -1
	},
	arrow: {
		backgroundColor: "transparent",
		borderColor: "transparent",
		borderWidth: 16,
		alignSelf: "center",
		marginTop: -32,
		zIndex: -1
	},
	topInfoContainer: {
		borderBottomWidth: 1,
		width: 190,
		paddingBottom: 5,
		flexDirection: "row",
		justifyContent: "space-between"
	},
	bottomInfoContainer: {
		marginTop: 10,
		flex: 1,
		flexDirection: "row"
	},
	topSmallInfoText: {
		fontSize: 15,
		color: palette.darkerGrey,
		fontWeight: "400"
	},
	bottomSmallInfoText: { color: theme.colors.active }
});
