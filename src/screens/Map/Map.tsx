import { StyleSheet, View } from "react-native";

import React, { useState } from "react";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import PropertyMarker from "./ApartmentMarker";
import MarkerInfo from "./MarkerInfo";
import { I18n } from "i18n-js";
import useGetMapProperties from "./useGetMapProperties";
import layout from "../../constants/Layout";

const regionCoordinates = {
	latitude: 52.520008,
	longitude: 13.404954,
	latitudeDelta: 0.025,
	longitudeDelta: 0.0221
};

interface MapProps {
	i18n: I18n;
	navigation: {
		navigate: (text: string, params?: { propertyId: number }) => void;
	};
}

export default function Map({ i18n, navigation }: MapProps) {
	const properties = useGetMapProperties();

	const [currentSelectedPropertyId, setCurrentSelectedPropertyId] =
		useState<number>();

	const currentSelectedProperty = properties.data?.find(
		(property) => property.id === currentSelectedPropertyId
	);
	return (
		<View style={styles.container}>
			<MapView
				provider={PROVIDER_GOOGLE}
				style={styles.map}
				moveOnMarkerPress={true}
				initialRegion={regionCoordinates}>
				{properties.data?.map((property) => (
					<PropertyMarker
						onPress={() =>
							setCurrentSelectedPropertyId(property.id)
						}
						selected={property.id === currentSelectedPropertyId}
						key={property.id}
						propertyDetails={property}
					/>
				))}
			</MapView>
			{currentSelectedProperty && (
				<MarkerInfo
					i18n={i18n}
					markerInfo={currentSelectedProperty}
					navigation={navigation}
				/>
			)}
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		height: layout.window.height,
		width: layout.window.width,
		justifyContent: "flex-end",
		alignItems: "center"
	},
	map: {
		...StyleSheet.absoluteFillObject
	}
});
