import { StyleSheet, View, Text } from "react-native";

import React from "react";
import { Marker } from "react-native-maps";

import { PropertyMapItem } from "../../common/propertyTypes";
import { theme, palette } from "../../constants/Theme";

interface ProperyMarkerProps {
	onPress: () => void;
	propertyDetails: PropertyMapItem;
	selected: boolean;
}
export default function PropertyMarker({
	onPress,
	propertyDetails: propertyDetails,
	selected
}: ProperyMarkerProps) {
	return (
		<Marker
			onPress={onPress}
			coordinate={{
				latitude: propertyDetails.position.latitude,
				longitude: propertyDetails.position.longitude
			}}>
			<View
				style={{
					...styles.markerStyle,
					...(selected ? styles.activeMarkerStyle : {})
				}}>
				<Text style={styles.markerTextStyle}>
					{propertyDetails.lowestPricePerNight}€
				</Text>
			</View>
			<View
				style={{
					...styles.arrowBorder,
					...(selected ? styles.selectedArrowBorder : {})
				}}
			/>
			<View
				style={{
					...styles.arrow,
					...(selected ? styles.selectedArrow : {})
				}}
			/>
		</Marker>
	);
}

const styles = StyleSheet.create({
	container: {
		...StyleSheet.absoluteFillObject,
		height: 950,
		width: 400,
		justifyContent: "flex-end",
		alignItems: "center"
	},
	map: {
		...StyleSheet.absoluteFillObject
	},
	markerStyle: {
		padding: 10,
		paddingTop: 11,
		paddingBottom: 11,
		backgroundColor: palette.darkerGrey,
		borderRadius: 5
	},
	activeMarkerStyle: {
		backgroundColor: theme.colors.active,
		borderRadius: 10
	},
	markerTextStyle: {
		fontSize: 16,
		color: theme.colors.primeryColor
	},
	arrowBorder: {
		backgroundColor: "transparent",
		borderColor: "transparent",
		borderWidth: 16,
		alignSelf: "center",
		marginTop: -10.5,
		zIndex: -1
	},
	selectedArrowBorder: {
		borderTopColor: palette.darkerOrange
	},
	arrow: {
		backgroundColor: "transparent",
		borderColor: "transparent",
		borderWidth: 16,
		alignSelf: "center",
		marginTop: -32,
		zIndex: -1,
		borderTopColor: palette.lighterGrey
	},
	selectedArrow: {
		borderTopColor: palette.darkerOrange
	}
});
