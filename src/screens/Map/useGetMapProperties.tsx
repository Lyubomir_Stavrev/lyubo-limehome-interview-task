import { useQuery } from "react-query";
import { getMappedProperties } from "../../services/services";

const useGetMapProperties = () =>
	useQuery("getMapProperties", getMappedProperties);

export default useGetMapProperties;
