import React, { useCallback } from "react";
import { ActivityIndicator, View, StyleSheet } from "react-native";
import useLoadFonts from "./useLoadFonts";
import * as SplashScreen from "expo-splash-screen";

SplashScreen.preventAutoHideAsync();

export default function (props) {
	const isFontLoaded = useLoadFonts();
	const isAppReady = isFontLoaded.isSuccess;
	const onLayoutRootView = useCallback(async () => {
		if (isAppReady) {
			await SplashScreen.hideAsync();
		}
	}, [isAppReady]);
	if (!isAppReady) {
		return (
			<ActivityIndicator
				accessibilityLabel="ActivityIndicator"
				size="large"
			/>
		);
	}
	return (
		<View style={styles.container} onLayout={onLayoutRootView}>
			{props.children}
		</View>
	);
}
const styles = StyleSheet.create({
	container: {
		flex: 1
	}
});
