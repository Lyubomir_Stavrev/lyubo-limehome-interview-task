
# Limehome - Coding Challenge #

React Native expo application that lists the available limehome apartment in Berlin on a MapView
## Warning ##
Supports android devices with version higher than 11
## Instalation ##

```
$ npm install
$ npx expo start
```
Then you can scan the QR code or use an emulator

### Run Jest Tests ###
```
$ npm run test
```
